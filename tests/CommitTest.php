<?php

require_once dirname(__file__).'/../vendor/autoload.php';

use Mojomaja\Component\Bserv\Client;

class CommitTest extends PHPUnit_Framework_TestCase
{
    public function testCommitOk()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('http://put.example.com/'),
                $this->equalTo([ 'article' => '@15371378661_a376c2c09b_k.jpg' ])
            )
            ->will($this->returnValue(json_encode([
                'article' => [ '2da8078877628bc85280c700aabe174fd19d195e.jpg' ]
            ])))
        ;

        $bserv = new Client('http://put.example.com/', 'http://get.example.com', $skurl);
        $this->assertEquals(
            '2da8078877628bc85280c700aabe174fd19d195e.jpg',
            $bserv->commit('15371378661_a376c2c09b_k.jpg')
        );
    }
}
