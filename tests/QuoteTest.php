<?php

require_once dirname(__file__).'/../vendor/autoload.php';

use \Mojomaja\Component\Bserv\Client;

class QuoteTest extends PHPUnit_Framework_TestCase
{
    public function testQuoteCrop()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $bserv = new Client('http://put.example.com/', 'http://get.example.com', $skurl);
        $this->assertEquals(
            'http://get.example.com/crop/400,300/2d/a8/2da8078877628bc85280c700aabe174fd19d195e.jpg',
            $bserv->quote('2da8078877628bc85280c700aabe174fd19d195e.jpg', 400, 300)
        );
    }
}
