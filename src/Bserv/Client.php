<?php

namespace Mojomaja\Component\Bserv;

class Client
{
    const SCHEMA_CROP = 'crop';
    const SCHEMA_FILL = 'fill';

    private $gateway_in;

    private $gateway_out;

    /**
     * @var \Mojomaja\Component\Skurl\Client
     */
    private $skurl;


    public function __construct($gateway_in, $gateway_out, \Mojomaja\Component\Skurl\Client $skurl)
    {
        $this->gateway_in   = $gateway_in;
        $this->gateway_out  = $gateway_out;
        $this->skurl        = $skurl;
    }

    public function commit($file)
    {
        return current($this->weave($this->skurl->post(
            $this->gateway_in,
            [ 'article' => "@{$file}" ]
        ))['article']);
    }

    public function quote($localname, $width = 0, $height = 0, $schema = self::SCHEMA_CROP)
    {
        if ($width != 0 || $height != 0)
            return sprintf(
                '%s/%s/%d,%d/%s/%s/%s',
                $this->gateway_out,
                $schema,
                $width,
                $height,
                substr($localname, 0, 2),
                substr($localname, 2, 2),
                $localname
            );
        else
            return sprintf(
                '%s/%s/%s/%s',
                $this->gateway_out,
                substr($localname, 0, 2),
                substr($localname, 2, 2),
                $localname
            );
    }

    private function weave($responseText)
    {
        $document = json_decode($responseText, true);
        if ($document === null)
            throw new Exception('cannot be decoded');

        if (!empty($document['error']))
            throw new Exception($document['message'], $document['error']);

        return $document;
    }
}
